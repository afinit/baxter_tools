#!/usr/bin/env python
import numpy as np
import cv2
import os
import sys
import argparse
import math

#################  
## POINT CLASS ##
#################

class point:
  def __init__(self, index, pos, dist=None, slope=None):
    self.index = index
    self.pos = tuple(pos)

    # dist and slope describe the line to the previous point
    self.dist = dist
    self.slope = slope
    self.width = None
    self.width_endpts = None

###############  
## END POINT ##
###############


################
## LINE CLASS ##
################

class line:
  def __init__(self):
    self.point_list = []
    self.length = 0
  
  def add_point(self, pos, dist=None, slope=None):
    self.point_list.append( point( len(self.point_list), pos, dist, slope ) )

    if dist != None:
      self.length += dist
    elif len(self.point_list) != 1:
      print 'Error: Could not find distance to previous point'
      sys.exit()

##############
## END LINE ##
##############


###################
## PANICLE CLASS ##
###################
class panicle:
  def __init__( self, img, check_reach, filename, csv_fh, init_step, verbose, step_prep, percent_area_mask ):
    self.img = img
    self.check_reach = check_reach
    self.filename = filename
    self.csv_fh = csv_fh
    self.init_step = init_step
    self.verbose = verbose
    self.step_prep = step_prep
    self.percent_area_mask = percent_area_mask

    self.threshold_img = None
    self.contour = None
    self.centerline = line()
    self.stem_found = True
    self.scale = 0.
    self.scale_pts = [(1,1),(2,2)]
    self.img_area = self.img.shape[0] * self.img.shape[1]
    
    # dimensions/attributes
    self.width = 0.
    self.curvature = 0.
    self.area = 0.
    self.width_pts = []
    self.avg_r = 0
    self.avg_g = 0
    self.avg_b = 0
    self.width_flag = False
    self.white_flag = False
    self.stem_problem_flag = False
    self.yellow_tape_flag = False 

  ###############################
  ## SUPPORT PANICLE FUNCTIONS ##
  ###############################
  
  # create a threshold image
  def threshold(self):
    # split out blue and green channels
    b = cv2.split(self.img)[0]
    g = cv2.split(self.img)[1]

    if self.percent_area_mask < 1:
      # subtract 1/4 of the green color from blue.. this removes a little more of the panicle without removing the sheet of paper if it's there
      mask_sheet_img = cv2.subtract( b,g/4 )
      
      # threshold image and find contours
      ret, mask_sheet_thresh = cv2.threshold(mask_sheet_img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
      conts, hierarchy = cv2.findContours(mask_sheet_thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
      
      # initialize variables for masking process
      min_area = self.img_area * self.percent_area_mask
      max_cont = None
      max_cont_area = 0

      # loop through contours found and retrieve the biggest image that is larger min_area
      for i in conts:
        i_area = cv2.contourArea( i )
        if i_area < self.img_area * .75 and i_area > min_area and i_area > max_cont_area:
          max_cont_area = i_area
          max_cont = i

      if not max_cont is None: 
        # mask out the scale tape from the image to help avoid issues with yellow scale tape
        rightmost = tuple(max_cont[max_cont[:,:,0].argmax()][0])[0]
        self.img = self.img[:,rightmost:]
        self.img_area = self.img.shape[0] * self.img.shape[1]

    # filter blue and green colors with GaussianBlur 
    b = cv2.split(self.img)[0]
    b = cv2.GaussianBlur(b, (11,11), 0)
    g = cv2.split(self.img)[1]
    g = cv2.GaussianBlur(g, (11,11), 0)
    
    # subtract 1/4 of the blue color from the green portion of the image.. this gets rid of much of the background color, but doesn't remove too much of the panicle itself
    color_manip = cv2.subtract(g,b/4)

    # using the Otsu Binariztation threshold algorithm, a background artifacts are limited much more
    ret, self.threshold_img = cv2.threshold(color_manip, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
  
  # return pts of intersection between line and contour
  def intersects( self, pt1, pt2, contour=None ):
    if contour is None:
      contour = self.contour

    # create two blank images
    line_img = np.zeros((self.img.shape[0], self.img.shape[1]), np.uint8)
    cont_img = line_img.copy()
    line_width = 2
    
    # build images for line and contour outline
    cv2.line( line_img, pt_int(pt1), pt_int(pt2), 255, line_width )
    cv2.drawContours( cont_img, [contour], 0, 255, 1 )

    # bound search area.. this greatly improves performance since the entire image doesn't have to be compared
    max_x = min(max( [pt1,pt2], key=lambda t: t[0] )[0] + line_width, self.img.shape[1])
    max_y = min(max( [pt1,pt2], key=lambda t: t[1] )[1] + line_width, self.img.shape[0])
    min_x = max(min( [pt1,pt2], key=lambda t: t[0] )[0], 0)
    min_y = max(min( [pt1,pt2], key=lambda t: t[1] )[1], 0)
    
    # cut images down to relevant areas
    line_img = line_img[min_y:max_y,min_x:max_x]
    cont_img = cont_img[min_y:max_y,min_x:max_x]

    # get intersection points
    line_img = cv2.bitwise_and( line_img, cont_img )
    
    # get a list of all nonzero points and their locations.. because of the way np.nozero() works, the results for this need to be transposed.. then the cropped 
    #   distances are added back to x and y so the points relate appropriately to the original image
    pts = np.transpose(np.nonzero(line_img)[::-1]) + (min_x,min_y)
    
    # return the points as a list as opposed to a numpy array
    return pts.tolist()


  # return the width of the contour at the index passed
  def contour_width(self, pt, slope):
    # get perpendicular slope
    slope = perp_slope( slope )
    intercept = get_intercept( pt, slope ) 

    # find points such that they are check_reach from the center of the panicle.. this is based on the scale calculation so should be an appropriate distance
    if slope == None:
      pt1 = (intercept, pt[1]-self.check_reach)
      pt2 = (intercept, pt[1]+self.check_reach)
    elif slope == 0:
      pt1 = (pt[0]-self.check_reach, intercept)
      pt2 = (pt[0]+self.check_reach, intercept)
    else:
      pt1, pt2 = self.endpoints( pt, (slope, intercept), self.check_reach )

    # get intersection point from the first side
    ints = self.intersects( pt, pt1 )
    if ints == []:
      self.width_flag = True
      return None, None, None
    elif len(ints) > 1 and ints.count(pt) > 0:
      ints.remove(pt)
    pt1 = max( ints, key=lambda i: distance( i, pt ) )

    # get intersection point from the second side
    ints = self.intersects( pt, pt2 )
    if ints == []:
      self.width_flag = True
      return None, None, None
    elif len(ints) > 1 and ints.count(pt) > 0:
      ints.remove(pt)
    pt2 = max( ints, key=lambda i: distance( i, pt ) )

    # return the distance and the endpoints of the width line
    return distance( pt1, pt2 ), (tuple(pt1), tuple(pt2))

  # find width of contour along centerline at the point indicated by index
  def centerline_width(self):
    # return if there are less than 3 points because this means there is an error finding points
    if len(self.centerline.point_list) < 3:
      return []
    
    # loop through the centerline points to calculate the width at each point
    #   use the average of the slope of the preceding and following lines
    for index in xrange(len(self.centerline.point_list)):
      center_pt = self.centerline.point_list[index]
      if index == 0:
        slope = self.centerline.point_list[index+1].slope
      elif index == len(self.centerline.point_list)-1:
        slope = center_pt.slope
      else:
        slope = get_slope( self.centerline.point_list[index+1].pos, self.centerline.point_list[index-1].pos )

      width_data = self.contour_width( center_pt.pos, slope )
      center_pt.width = width_data[0]
      center_pt.width_endpts = width_data[1]
    
    # sort points based on width
    points_sorted = sorted( self.centerline.point_list, key=lambda p: p.width, reverse=True )
    
    # return [] if the width can't be found at 3 points
    if None in [ points_sorted[i].width for i in [0,1,2]]:
      return []
    
    # return the top 3 points
    return points_sorted[:3]
     
  # find next point using two points (pt1 is the last pt) and distance to travel
  def travel_x(self, pt1, pt2, dist):
    d = -distance( pt1, pt2 )
    return ( int(pt1[0] + (dist/d)*(pt2[0]-pt1[0])), int(pt1[1] + (dist/d)*(pt2[1]-pt1[1])) )

  # check which side of the line the point is on, returns -1, 0, 1
  #   this is not written to work for vertical lines
  def pt_pos(self, pt, m, intercept ):
    if m is None:
      if pt[0] > intercept:
        return 1
      elif pt[0] < intercept:
        return -1
      else:
        return 0
      
    pos = m * pt[0] + intercept

    if( pos > pt[1] ):
      return -1
    elif( pos == pt[1] ):
      return 0
    else:
      return 1

  # return endpoints
  def endpoints(self, last_pt, line, check_reach ):
    if line[0] == None:
      ref_pt = ( line[1], 0 )
    else:
      ref_pt = ( 0, line[1] )

    return [self.travel_x( last_pt, ref_pt, check_reach ), self.travel_x( last_pt, ref_pt, -check_reach )]

  # check the new point to see if it is far enough and in the correct direction
  def check_pt(self, new_pt, pt1, pt2):
    # angle within which to check to verify correct direction, in radians
    check_angle = 1.75
    m = perp_slope( get_slope( pt1, pt2 ) )
    
    # set slope bounds using math
    if m == None:
      m1 = 1.
      m2 = -1.
    else:
      m1 = math.tan( math.atan( m ) + check_angle/2 ) 
      m2 = math.tan( math.atan( m ) - check_angle/2 ) 

    # determine if a slope is supposed to be undetermined (vertical)
    if np.fabs(m1) > 10000 or np.fabs(m2) > 10000:
      m1 = None
      m2 = 0.
    
    # get the intercepts for each slope so the lines can be described
    intcpt1 = get_intercept( pt2, m1 ) 
    intcpt2 = get_intercept( pt2, m2 ) 

    # get relative position of new_pt
    new_pos1 = self.pt_pos( new_pt, m1, intcpt1 )
    new_pos2 = self.pt_pos( new_pt, m2, intcpt2 )

    # get relative position of pt1
    pt1_pos1 = self.pt_pos( pt1, m1, intcpt1 )
    pt1_pos2 = self.pt_pos( pt1, m2, intcpt2 )

    # compare the relative positions of the points and return true if they are in the same quadrant.. if the new_pt is on one of the lines 
    #   it only matters what side of the other line the points are on.. lines are included here, not excluded
    if new_pos1 == 0:
      return new_pos2 == pt1_pos2
    elif new_pos2 == 0:
      return new_pos1 == pt1_pos1
    else:
      return new_pos1 == pt1_pos1 and new_pos2 == pt1_pos2

  # return image containing only contour section of focus
  def section_img( self, pt1, pt2 ):
    # get section contour by using a bitwise_and on the contour and the shape used as a section
    sect_img = np.zeros(self.threshold_img.shape, np.uint8)
   
    # get orthogonal lines in point slope form
    m = perp_slope( get_slope( pt1, pt2 ) )
    intcpt1 = get_intercept( pt1, m )
    intcpt2 = get_intercept( pt2, m )

    # get endpoints for the orthogonal lines
    endpts = []
    endpts.append( self.endpoints(pt1, (m, intcpt1), self.check_reach) )
    endpts.append( self.endpoints(pt2, (m, intcpt2), self.check_reach) )

    # create a contour out of the endpoints and draw it on a blank image
    shape_cont = np.array([endpts[0][0], endpts[1][0], endpts[1][1], endpts[0][1]])
    cv2.drawContours( sect_img, [shape_cont], 0, 255, -1 )

    # and endpoints contour with the threshold_img 
    sect_img = cv2.bitwise_and( sect_img, self.threshold_img )
    return sect_img
    
  # function to find the center of mass of the section of the contour between the two lines defined by the points passed and the slope perpendicular to the line passing through those two points
  def next_pt( self, pt1, pt2, radius ):
    if radius == -1:
      temp_pt = pt2
    else:
      temp_pt = self.travel_x( pt1, pt2, radius )

    # get the section img and calculate the moments to find the center of mass
    sect_img = self.section_img( pt1, temp_pt ) 
    mom = cv2.moments( sect_img, True )
    if mom['m00'] == 0:
      return None, None

    # math
    x = int( mom['m10']/mom['m00'] )
    y = int( mom['m01']/mom['m00'] )
    return (x,y), temp_pt

  # find the bottom of the panicle and then reset the contour to disclude that section
  def panicle_bottom( self ):
    # set step based on the scale.. 250 is just a number that seems to work well after some trial and error
    #   a value too small here leads to weird slopes between the points, too large and it leads to travelling
    #   too far and missing data points
    step = int( 250 * self.scale )

    # set the stem_bottom and panicle_top
    stem_bottom = tuple(self.contour[self.contour[:, :, 1].argmax()][0])
    panicle_top = tuple(self.contour[self.contour[:, :, 1].argmin()][0])
    mean_width = 0.
    total = 0.

    # calculate initial points
    last_pt2, blank_pt = self.next_pt( (stem_bottom[0], stem_bottom[1]-int(step*self.init_step)), stem_bottom, 1 )
    bottom = last_pt2
    last_pt = (last_pt2[0], last_pt2[1]-1)

    # loop until the bottom of the panicle is found, or an issue comes up with the next point resulting in the bottom of the panicle not being found and the bottome of the contour being used instead
    while(True):
      point, blank_pt = self.next_pt( last_pt, last_pt2, step )
      
      # check if point was returned
      if point is None or point[1] < panicle_top[1] + step*3 or point[1] > last_pt[1]:
        if self.verbose:
          print '\tNo Stem Found'
        self.stem_found = False
        break

      # get slope to define line, and then get the width at this point
      m = get_slope( point, last_pt )
      width = self.contour_width( point, m )

      # check if width was returned
      if width[0] is None:
        if self.verbose:
          print '\tNo Stem Found'
        self.stem_found = False
        break

      ## CHECK IF WIDTH HAS CHANGED DRASTICALLY TO CALL PANICLE BOTTOM
      # if the width suddenly jumps up to more than twice the average width from the previous points, this is likely the bottom of the panicle
      #   total needs to be at least 2, so an average can be calculated.. the first point will sometimes be in a spot that is narrower than the rest of the stem
      if total < 2 or width[0] <= mean_width*2.:
        mean_width = ((mean_width * total) + width[0]) / (total + 1)
        total += 1
        last_pt2 = last_pt
        last_pt = point
      # if the width has jumped up, set bottom to the current point
      else:
        last_pt2 = last_pt
        last_pt = point
        bottom = last_pt2
        point, blank_pt = self.next_pt( bottom, stem_bottom, step )
        last_pt = point

        # loop thru 50 more points or until the next point is less than 4 steps from the top of the panicle
        #   this gives the program an opportunity to attempt to find whether the stem continues above this point
        #   thru this process, the mean_width remains the same as before, and the next points follow the line
        #   passing thru the last 2 points from the previous section.. this ensures the points continue up the panicle
        #   and don't go off on a random tuft and get stuck
        for extra_step in xrange( 50 ):
          point, blank_pt = self.next_pt( last_pt, stem_bottom, step )
          if point is None:
            break
          
          # check if the point is within 4 steps of the top of the panicle
          if point[1] < panicle_top[1] + step*4:
            break

          # this is the code that creates the next point based on a straight line from the lsat points of the stem
          temp_pt = self.travel_x( last_pt, stem_bottom, step )
          if temp_pt[1] < step:
            break
          last_pt2 = last_pt
          last_pt = temp_pt
          width = self.contour_width( point, m )
          
          # this is more constricted than before to prevent something being mistaken as stem 
          if width[0] <= mean_width*1.75:
            bottom = point
          
        break

    # crop the stem from the panicle and find the contour again
    contour_img = self.threshold_img[ :bottom[1] ]
    conts, hierarchy = cv2.findContours(contour_img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    
    # catch error of no contour found after image crop
    if conts == []:
      print 'Error: problem processing panicle stem'
      self.stem_problem_flag = True
      return None

    # store new contour after removing the stem
    self.contour = max(conts, key=lambda i: cv2.contourArea(i) )
    return bottom

  #################
  ## END SUPPORT ##
  #################


  ##############
  ###############################
  ## PRIMARY PANICLE FUNCTIONS ##
  ###############################

  ###################################################
  ## CALIBRATE() METHOD
  # calibrate image based on the piece of tape located in the images to calculate the scale value
  def calibrate( self, scale_key ):
    # We find the contour of the red tape, find the bottommost point, draw a vertical line, find the point where that
    # line intersects the top edge of the tape, find the distance between those two points, and use that as our
    # calibration
    min_area = 500
    
    # find calibration tape if red
    g = cv2.split(self.img)[1]
    r = cv2.split(self.img)[2]
    calibrationImg = cv2.subtract(r, g) 
    calibrationImg = cv2.blur(calibrationImg,(3,3))
    ret, thresh = cv2.threshold(calibrationImg, 60, 255, cv2.THRESH_BINARY)
    contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    nocali_marker=1
    for cnt in contours:
      if cv2.contourArea(cnt)< min_area:
        continue
      else:
        nocali_marker=0

    # find calibration tape if yellow
    if nocali_marker==1: ## solve the yellow marker issue
      self.yellow_tape_flag = True
      b = cv2.split(self.img)[0]
      calibrationImg = cv2.add(cv2.subtract(g, r/4), b)
      calibrationImg = cv2.blur(calibrationImg,(3,3))
      ret, thresh = cv2.threshold(calibrationImg, 170,255, cv2.THRESH_BINARY)
      contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    max_x=0
    # loops through the contours found and pulls out the rightmost contour
    for cnt in contours:
      if cv2.contourArea(cnt) > min_area:
        rightmost=tuple(cnt[cnt[:,:,0].argmax()][0])[0]
        if rightmost>max_x:
          max_x=rightmost
          rightmost_cnt=cnt
    cnt=rightmost_cnt

    # Find the bottommost point
    Bottommost = tuple(cnt[cnt[:,:,1].argmax()][0])

    # Create a vertical line and find the point(s) of intersection with the sticker to find Topmost point
    Img1 = np.zeros((calibrationImg.shape[0], calibrationImg.shape[1]), np.uint8)
    Img2 = Img1.copy()
    cv2.drawContours(Img1, [cnt], -1, 1, 2)
    cv2.line(Img2, pt_int(Bottommost), (int(Bottommost[0]), 0), 1, 1)
    calibrationIntersection = np.bitwise_and(Img1, Img2)
    pts = np.transpose(np.nonzero(calibrationIntersection)[::-1])
    Topmost = min( pts, key = lambda t: t[1] )

    # mask out the scale tape from the image to help avoid issues with yellow scale tape
    cv2.drawContours(self.img,[cnt],0,0,-1)

    # Take the distance between the points as our calibration -- "mm"
    self.scale = float(scale_key) / (Bottommost[1] - Topmost[1])
    
    # calculate check_reach as 2/3 the height of the tape
    self.check_reach = (Bottommost[1] - Topmost[1])*2./3
    self.scale_pts = [Bottommost, Topmost]

  
  ###################################################
  ## FIND_PANICLE() METHOD
  # find the panicle in the image by processing through each of the contours discovered in the threshold image
  def find_panicle(self):
    # create threshold image
    self.threshold()

    # find contours in the threshold image
    contours, hierarchy = cv2.findContours(self.threshold_img,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    max_area = 0

    # loop through the contours pulling the one with the greatest area unless it sits against the left edge of the image 
    #   or has a color average higher than 175.. a safe value that can weed out a few of the papers
    for c in contours:
      tmp_area = cv2.contourArea(c)
      if tmp_area > max_area and tmp_area < self.img_area * .5 and self.intersects( (0,0), (0,self.img.shape[1]), c) == []:
        mask = np.zeros((self.img.shape[0], self.img.shape[1], 1),np.uint8)
        cv2.drawContours(mask,[c],0,255,-1)
        if np.average( cv2.mean( self.img, mask=mask )[:3] ) < 185:
          self.contour = c
          max_area = tmp_area

    # return 0 for success
    if self.contour is None:
      return -1
    else:
      # create solid contour image
      self.threshold_img = np.zeros(self.threshold_img.shape, np.uint8)
      cv2.drawContours( self.threshold_img, [self.contour], 0, 255, -1 )

  
  ###################################################
  ## FIND_CENTERLINE() METHOD
  # find and store the points making up the centerline
  def find_centerline(self, no_stem):
    # find stem unless otherwise specified at the command line
    if no_stem:
      bottom_point = tuple(self.contour[self.contour[:, :, 1].argmax()][0])
    else:
      bottom_point = self.panicle_bottom()
    
    # if an error occurred while trying to find the bottom point of the contour, bottom_point will be None
    if bottom_point is None:
      return False

    #### POINT VARIABLES USED IN THIS SECTION
    #   last_pt is the last established point
    #   last_pt2 is the second to last established point (at initialization it is set at the very bottom of the panicle but isn't actually used)
    #   cm_pt is the center of mass point found using self.next_pt()
    #   temp_pt is the projected point that is created by projecting a line through the last 2 points and continuing a distance of step
    # initialize last_pt and last_pt2 to process the centerline of the panicle
    last_pt2 = bottom_point
    last_pt = (last_pt2[0], last_pt2[1]-5)
    
    # initialize the centerline with the bottom point
    self.centerline.add_point(last_pt)
    
    # set the step value.. this is set as a fraction of the check_reach to limit the distance between each point
    #   for those interested in playing with or modifying this value, it is set in main(), but keep in mind the fraction shouldn't be too small
    #   because this will make the distance between points too short, this causes the variability of the edges of the panicle contour to have too
    #   much influence over the lateral placement of the next point
    step = self.check_reach*self.step_prep

    # process the panicle by moving along the panicle
    #   loop until a break since there are several points at which the loop could be done
    while(True):
      # get next point as the center of mass of the section of the panicle between last_pt and temp_pt
      cm_pt, temp_pt = self.next_pt( last_pt, last_pt2, step )

      # make sure cm_pt is not None which would indicate an issue getting the next point
      if not cm_pt is None:
        # make sure the next point is at least 1/4 the temp_pt distance
        dist = distance( cm_pt, last_pt )
        if dist < step/4:
          cm_pt = self.travel_x( cm_pt, last_pt, step/4 - dist )
      else:
        cm_pt = self.travel_x( last_pt, last_pt2, step/4 )
        # if cm_pt is None, so is temp_pt.. this line ensures that temp_pt has a value for check_pt
        temp_pt = cm_pt

      # check to ensure the point is in the correct direction, and still within the contour
      if not self.check_pt( cm_pt, temp_pt, last_pt ) or cv2.pointPolygonTest(self.contour, cm_pt, False) < 0:
        # if one of these conditions was not met, set temp_pt to be much further to check whether the projected line crosses the contour at the end
        temp_pt = self.travel_x( last_pt, last_pt2, self.check_reach * 2 )
        
        # check whether the projected line crosses the contour at the end.. temp_pt is used here because at the end of the contour, the points are more likely to go in a weird direction since less data is being included in the 
        #   smaller section at the tip of the panicle.. the line passing through the last two points is more dependable than the cm_pt at this point
        intersection_pts = self.intersects( temp_pt, last_pt )
        
        # if the projected line did not cross the contour, this indicates an error.. the last point will be drawn on the image to help in troubleshooting 
        if intersection_pts == []:
          cv2.circle( self.img, cm_pt, 7, (0,255,255), -1 ) 
          print 'Error finding last point in ' + self.filename
          break
        else:
          # add the intersection point to the point list
          self.centerline.add_point(intersection_pts[0], distance(intersection_pts[0],last_pt), get_slope(intersection_pts[0],last_pt))
          return True
      else:
        # if the check conditions are passed, add cm_pt to the point list and change the value of last_pt and last_pt2 to match the list
        last_pt2 = last_pt
        last_pt = cm_pt
        self.centerline.add_point(last_pt, distance(last_pt,last_pt2), get_slope(last_pt,last_pt2))
    return True
 

  ###################################################
  ## CALC_DIMENSIONS() METHOD
  # Calculate: length, width, area, curvature, and the average RGB values within the panicle
  def calc_dimensions(self):
    # retrieve the 3 points with the largest widths
    self.width_pts = self.centerline_width()
    if len( self.width_pts ) < 3:
      print 'Error: less than 3 center points found'
      return False
    else:
      # average the widths of the 3 widest points to account for the variablility of the sides of the panicle
      self.width = sum( [i.width for i in self.width_pts] )/3 * self.scale

    # calculate the curvature
    total_pts = 0
    total_dist = 0
    angle_dif = 0
    
    # pull and loop over all of the points except the first point and the last because these points can skew the curvature
    pt_lst = self.centerline.point_list[1:-1]
    for i in xrange(len(pt_lst)-1):
      total_pts += 1
      total_dist += pt_lst[i].dist
      
      # calculate the first angle
      slp1 = pt_lst[i].slope
      if slp1 is None:
        ang1 = np.arctan( np.inf )
      else:
        if slp1 < 0:
          ang1 = np.arctan( slp1 ) + np.pi
        else:
          ang1 = np.arctan( slp1 )
      slp2 = pt_lst[i+1].slope
      
      # calculate the second angle
      if slp2 is None:
        ang2 = np.arctan( np.inf )
      else:
        if slp2 < 0:
          ang2 = np.arctan( slp2 ) + np.pi
        else:
          ang2 = np.arctan( slp2 )

      angle_dif += np.fabs(ang2-ang1)/pt_lst[i].dist

    # check to make sure there have been points collected to avoid division by 0 and the end of the world
    if total_pts > 0:
      self.curvature = angle_dif/total_pts/self.scale
    else:
      if self.verbose:
        print 'Error: Could not find curvature. Not enough points'
      self.curvature = -1

    # calculate rgb values within the selected area of the panicle
    mask = np.zeros((self.img.shape[0], self.img.shape[1], 1),np.uint8)
    cv2.drawContours(mask,[self.contour],0,255,-1)
    avg_color = cv2.mean( self.img, mask=mask )
    self.avg_r = avg_color[2]
    self.avg_g = avg_color[1]
    self.avg_b = avg_color[0]
    
    # calculate length and width
    self.area = cv2.contourArea( self.contour ) * (self.scale ** 2)
    self.centerline.length = self.centerline.length * self.scale
    
    # print the following values to screen when verbose is selected at the command line
    if self.verbose:
      print 'width: {}'.format( self.width )
      print 'length: {}'.format( self.centerline.length )
      print 'area: {}'.format( self.area )
      print 'curvature: {}'.format( self.curvature )
      print 'avg_color: {} '.format( avg_color )
    return True


  ###################################################
  ## WRITE_CALC() METHOD
  # write calculations to the csv file
  def write_calc( self, flag ):
    if self.width_flag == True:
      flag += '+width_flag'
    if self.white_flag == True:
      flag += '+white_flag'
    if self.stem_problem_flag == True:
      flag += '+stem_problem_flag'
    if self.yellow_tape_flag == True:
      flag += '+yellow_tape_flag'

    self.csv_fh.write( self.filename + ', ' + str(self.width) + ', ' + str(self.centerline.length) + ', ' + str(self.area) + ', ' + str(self.curvature) + ', ' + str(self.avg_r) + ', ' + str(self.avg_g) + ', ' + str(self.avg_b) + ', ' + str(self.scale) + ', ' + str(self.scale_pts) + ', ' + str(self.stem_found) + ', ' + flag + '\n' )


  ###################################################
  ## DRAW_ELEMENTS() METHOD
  # draw width lines, centerline points, and contour that were used in the calculations.. then the size of the image is reduced by half to save disk space
  def draw_elements(self, filepath):
    print_img = self.img.copy()
    cv2.drawContours(print_img, [self.contour], 0, (0,255,255), 2)
    
    # draw width lines
    for i in self.width_pts:
      cv2.line( print_img, pt_int(i.width_endpts[0]), pt_int(i.width_endpts[1]), (255,255,0), 5 )
    
    # draw centerline points
    for i in self.centerline.point_list:
      cv2.circle( print_img, i.pos, 7, (255,0,255), -1 ) 
    
    # draw scale points and line
    cv2.circle( print_img, pt_int(self.scale_pts[0]), 9, (255,0,0), -1 ) 
    cv2.circle( print_img, pt_int(self.scale_pts[1]), 9, (255,0,0), -1 ) 
    cv2.line( print_img, pt_int(self.scale_pts[0]), pt_int(self.scale_pts[1]), (255,255,0), 5 )
        
    print_img = cv2.pyrDown( print_img )
    cv2.imwrite(filepath, print_img)

  #################
  ## END PRIMARY ##
  #################


#################
## END PANICLE ##
#################


## **


############################
## GENERAL MATH FUNCTIONS ##
############################

# return distance between two points in float format
def distance( pt1, pt2 ):
  return math.sqrt( (pt1[0]-pt2[0])**2 + (pt1[1]-pt2[1])**2 )

# return midpoint of points passed
def midpoint( pt1, pt2 ):
  return ( (pt1[0]+float(pt2[0]))/2, (pt1[1]+float(pt2[1]))/2 )

# returns y-intercept unless m == None, then returns x-intercept
def get_intercept( pt, m ):
  if m == None:
    return pt[0]
  else:
    return pt[1] - m * pt[0]

# returns the perpendicular slope
def perp_slope( m ):
  if m == 0:
    return None
  elif m == None:
    return 0
  else:
    return -1/m

# return slope between the two points
def get_slope( pt1, pt2 ):
  if( pt1[0] == pt2[0] ):
    m = None
  else:
    m = (float(pt1[1])-pt2[1])/float(pt1[0]-pt2[0])

  return m

def pt_int( pt ):
  return int(pt[0]), int(pt[1])

#################
## END GENERAL ##
#################



def main( prog_name, argv ):
  out_dir = 'panicle_out'
  prog_name = os.path.basename( prog_name )

  # ARG PROCESSING
  parser = argparse.ArgumentParser( prog=prog_name, description='Process images to find the length, width, and area of the panicle in each image.',
      formatter_class=argparse.ArgumentDefaultsHelpFormatter )
  parser.add_argument('infile', metavar='INFILE', type=str, nargs='+', help='image files to be processed')
  parser.add_argument('-o,--out_dir', dest='_dir', metavar='DIR', type=str, default=out_dir, help='directory to save output to')
  parser.add_argument('-s,--scale_key', dest='scale_key', metavar='KEY', type=int, default=20, help='length of the scale in mm, used for scaling the image')
  parser.add_argument('-p,--scale_pix', dest='scale_pix', metavar='PIXELS', type=int, default=-1, help='manual entry for scale length in pixels. this is useful when the scale tape is measured wrong')
  parser.add_argument('-l,--left_crop', dest='left_crop', metavar='LCROP', type=float, default=0., help='fraction of image to crop from the left side of the image in decimal form')
  parser.add_argument('-r,--right_crop', dest='right_crop', metavar='RCROP', type=float, default=0., help='fraction of image to crop from the right side of the image in decimal form')
  parser.add_argument('-t,--top_crop', dest='top_crop', metavar='TCROP', type=float, default=0., help='fraction of image to crop from the top side of the image in decimal form')
  parser.add_argument('-b,--bottom_crop', dest='bottom_crop', metavar='BCROP', type=float, default=0., help='fraction of image to crop from the bottom side of the image in decimal form')
  parser.add_argument('-m,--percent_mask', dest='perc_mask', metavar='PERC_MASK', type=float, default=.15, help='minimum percentage of the total image area to consider a contour when attempting to mask out the sheet present in most images')
  parser.add_argument('-i,--init_step', dest='init_step', metavar='ISTEP', type=float, default=1., help='fraction of scaled step to take initially from the bottom')
  parser.add_argument('-n,--no_stem', dest='no_stem', action='store_const', const=True, default=False, help='skip process of finding stem and simply use the bottom of the contour')
  parser.add_argument('-v,--verbose', dest='verbose', action='store_const', const=True, default=False, help='print additional information to screen while processing')
  #parser.add_argument('-z,--test', dest='test', action='store_const', const=True, default=False, help='print additional images at intermediary points while processing to help figure out what is happening at each step')
  args = parser.parse_args(argv)

  # STORE ARGUMENT PARAMETERS
  infile = args.infile
  out_dir = args._dir
  scale_key = args.scale_key
  scale_pix = args.scale_pix
  no_stem = args.no_stem
  verbose = args.verbose
  #test_run = args.test
  lcrop = args.left_crop
  rcrop = args.right_crop
  tcrop = args.top_crop
  bcrop = args.bottom_crop
  percent_area_mask = args.perc_mask
  init_step = args.init_step
  step_prep = 2./3

  # make sure out_dir path is absolute
  if not os.path.isabs(out_dir):
      out_dir = os.getcwd() + os.sep + out_dir

  # if out_dir doesn't exist, create it
  if not os.path.isdir(out_dir):
      if verbose:
          print out_dir + ' created'
      os.makedirs(out_dir)
  if not os.path.isdir(out_dir + os.sep + 'find_centerline_fail'):
    os.makedirs(out_dir + os.sep + 'find_centerline_fail')
  if not os.path.isdir(out_dir + os.sep + 'calc_dimensions_fail'):
    os.makedirs(out_dir + os.sep + 'calc_dimensions_fail')
  if not os.path.isdir(out_dir + os.sep + 'yellow_tape'):
    os.makedirs(out_dir + os.sep + 'yellow_tape')
  if not os.path.isdir(out_dir + os.sep + 'panicle_area_issues'):
    os.makedirs(out_dir + os.sep + 'panicle_area_issues')
  if not os.path.isdir(out_dir + os.sep + 'width_issues'):
    os.makedirs(out_dir + os.sep + 'width_issues')
  if not os.path.isdir(out_dir + os.sep + 'region_ignore'):
    os.makedirs(out_dir + os.sep + 'region_ignore')
  if not os.path.isdir(out_dir + os.sep + 'no_issue'):
    os.makedirs(out_dir + os.sep + 'no_issue')

  # open csv file for writing to
  csv_name = out_dir + os.sep + 'panicle_out.csv'
  csv_fh = open( csv_name, 'w' )
  csv_fh.write( '## filename, width(cm), length(cm), area(cm^2), curvature(mm^-1), avg_r, avg_g, avg_b, scale, scale_pts, stem_found, flag \n' )

  # loop thru input files
  for f in infile:
    # parse out filename
    filename = os.path.basename(f)
    filename = os.path.splitext(filename)[0]
    print 'Processing: ' + filename

    # prep image
    proc_img = cv2.imread(f, 1)
    img_shape = proc_img.shape
    
    if rcrop > 0:
      proc_img = proc_img[:,:img_shape[0]*(-rcrop)]
    if lcrop > 0:
      proc_img = proc_img[:,img_shape[0]*lcrop:]
    if bcrop > 0:
      proc_img = proc_img[:img_shape[1]*(-bcrop),:]
    if tcrop > 0:
      proc_img = proc_img[img_shape[1]*tcrop:,:]

    #####################
    ## PROCESS PANICLE ##
    #####################
    
    pncl = panicle(proc_img, 200, filename, csv_fh, init_step, verbose, step_prep, percent_area_mask)
    
    if scale_pix > 0:
      # Take the  -- "mm"
      pncl.scale = float(scale_key) / scale_pix
      # calculate check_reach as 2/3 the height of the tape
      pncl.check_reach = scale_pix*2./3
    else:
      pncl.calibrate( scale_key )
    
    if pncl.find_panicle() == -1:
      pncl.write_calc( 'no_panicle' )
      print 'Error: no panicle found'
      continue
    
    if not pncl.find_centerline(no_stem):
      pncl.draw_elements( out_dir + os.sep + 'find_centerline_fail' + os.sep + filename + ".jpg")
      pncl.write_calc( 'find_centerline' )
      print "Failed at find_centerline() process"
      print "Wrote image to find_centerline_fail"
      continue
    
    if not pncl.calc_dimensions():
      pncl.draw_elements( out_dir + os.sep + 'calc_dimensions_fail' + os.sep + filename + '.jpg')
      pncl.write_calc( 'calc_dimensions' )
      print "Failed at calc_dimensions() process"
      print "Wrote image to calc_dimensions_fail"
      continue
    
    # calculate the area described by length * width of the panicle.. not perfect but it will categorize images that may need extra attention
    area_test = pncl.width * pncl.centerline.length
    if pncl.yellow_tape_flag:
      pncl.write_calc( '' )
      print 'Wrote image to yellow_tape'
      pncl.draw_elements( out_dir + os.sep + 'yellow_tape' + os.sep + filename + '.jpg')
    elif pncl.area > .95 * area_test or pncl.area < .65 * area_test:
      pncl.write_calc( 'panicle_area' )
      print 'Wrote image to panicle_area_issues'
      pncl.draw_elements( out_dir + os.sep + 'panicle_area_issues' + os.sep + filename + '.jpg')
    elif pncl.width_flag:
      pncl.write_calc( '' )
      print 'Wrote image to width_issues'
      pncl.draw_elements( out_dir + os.sep + 'width_issues' + os.sep + filename + '.jpg')
    elif pncl.white_flag:
      pncl.write_calc( '' )
      print 'Wrote image to region_ignore'
      pncl.draw_elements( out_dir + os.sep + 'region_ignore' + os.sep + filename + '.jpg')
    else:
      pncl.write_calc( 'none' )
      pncl.draw_elements( out_dir + os.sep + 'no_issue' + os.sep + filename + "_final_img.jpg")
    

  # close csv file
  csv_fh.close()


if __name__ == '__main__':
  main(sys.argv[0], sys.argv[1:])
