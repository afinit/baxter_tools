  /################################################/
 //################################################//
///#### A WALKTHROUGH OF DEVELOPMENT DECISIONS ####///
 //################################################//
  /################################################/

################
## PANICLE.PY ###
##################

When this program runs, an output directory, named panicle_out by default and placed in the current directory, with 6 subdirectories will 
  be created. As the program runs, processed images are output into these directories depending on what happens during the process. If the 
  imaged is flagged for some reason, it will end up in one of the subdirectories. If it is not flagged, it will end up in the main output 
  directory, although most images are flagged for something. The subdirectories are as follows:
    1. find_centerline_fail - if the program fails during the find_centerline process
    2. calc_dimensions_fail - if the program fails to calculate, usually due to a lack of data points
    3. yellow_tape - if the program detects yellow tape as the scale tape
    4. panicle_area_issues - if the area of the panicle falls outside the bounds of 60% and 90% of the length times width calculated area
    5. width_issues - if the program is not able to find an outside point when calculating the width at some point
    6. region_ignore - if the program disregards one of the bigger contours for some reason

  Most of the flagged images, except those in the directories marked fail, will be fine, but are placed into the flagged directories for
  review. Most of the images will wind up in region_ignore because the white paper in most of the images will be removed. This is still
  an important directory to glance through in case the panicle was the contour ignored.

  Issues that could come up:
    - The stem is not found: This could be solved by cropping either part or all of the stem.
    - The pin across the stem can sometimes be included in the panicle contour and cause issues this way. Cropping out the pin should solve this.
    - If the paper is picked as the panicle contour, cropping the left of the image to the point that the paper at least runs along the
        image side should solve this issue.
    - If the image is too dark and only part of the panicle is found: There is no fix for this right now.
    - If one of the tufts at the bottom of the panicle is followed as the panicle: This can be solved by cropping out that tuft.


The panicle analysis program can be broken into 5 basic steps:
  1. Image Calibration
  2. Panicle Discovery
  3. Centerline Plotting
  4. Calculation
  5. Output

# Image Calibration
  The calibrate() method of panicle is designed to discover the reference scale tape in the image and calculate the number
  of pixels per mm in the image. This is accomplished through a number of steps:
    1. get the red and green values of the image
    2. subtract the green from the red to get rid of most of the background noise
      2a. if the tape is yellow, an attempt is made to address yellow tape being used as the scale tape. There are usually
          always some issues with the images with yellow tape at this point due to the fact that yellow and green are closely
          related. This is likely best addressed by entering the scale length manually and using the cropping options to 
          crop out the yellow tape
    3. find the rightmost contour that is bigger than min_area (currently hardcoded to 500)
    4. draw a line from the bottommost point of the tape straight up
    5. find the topmost intersection point
    6. use the bottommost point and the intersection point as the scale length points
    7. calculate scale as the scale_key / scale_len, which gives units of mm/pixel
    8. calculate check_reach as scale_len * 2/3, check_reach is used as the distance fromthe centerline out to find the
        outside line of the panicle at that point. The reason for making this 2/3 is to limit it a little in case the panicle
        happens to curve over enough that a line could run into the other side. It does need to be somewhat long though
        to be inclusive. Also, other values are calculated off of this number.
    9. change the pixels of the scale tape to 0

# Panicle Discovery
  The find_panicle() method has three basic steps, first the image is checked for a piece of paper, and if found, it is cropped out. Second,
  the image is thresholded and then the panicle is found among the contours in the image. The image is thresholded by blurring the blue and 
  green images, then subtracting 1/4 of the blue image values from the green image values. This approach gets rid of the background noise without 
  removing too much of the panicle.

  Once thresholded, the image is searched for contours. These contours are then processed to find the contour with the biggest area that is
    not on the left edge and does not have an average color value of 175. This value was settled on as a value that would not eliminate any
    of the panicles, but would restrict at least some of the papers that are not along the left edge.

  After the best guess contour is found, this contour is printed white on a black background to threshold_img. This image is used for masking
    later on.

# Centerline Plotting
  The find_centerline() method is probably the most complex part of this process, and if there is anywhere this program can most easily be improved,
    it is probably here. It seems like it may be a little overcomplicated at times, but it works and it should have ample comments. This method is 
    designed to first find the bottom of the panicle by tracing the stem and finding the point where the width jumps significantly. This jump will
    indicate the bottom of the panicle. It will then check to make sure that what it found was not an errant tuft by continuing in the same direction
    checking to see if the panicle returns to stem width. If necessary the bottom point will be moved.
    
      **NOTE: It is important to note here that the find_bottom() portion can be skipped if trying to process a finicky image. 

    The program will then follow the path of the panicle from the bottom point to the end by using sections of the panicle between the last point found
    and a projected point. The center of mass of these sections will be used as the next point. At the end of the panicle, the points will begin getting 
    closer together, and have the potential to go in odd directions, as they conform to the center of mass calculations. If the center of mass point is
    determined to be less than 1/4 of the projected point distance, the point will be forced in the direction of the center of mass point to a distance
    1/4 the projected point distance. If the point is determined to be more than 45 degrees to the left of the projected path or is not in the contour, 
    the centerline loop is exited. The program will then find a new projected point along the line passing through the last 2 points, and determine if 
    the line between that point and the last point crosses the contour. If so, this point will be the last of the centerline. If it does not cross the
    contour, an error is raised and the image will be printed to the find_centerline_fail directory.

  This functionality is further described here:

  variables used:
    - last_pt: the last point found and selected
    - last_pt2: the second to last point found and selected
    - cm_pt: center of mass point found in the selected area of the contour
    - temp_pt: temp value
    - point: the next point to examine
    
  panicle_bottom():
    1. create a step value based on the scale value: this is to help the process proceed along the stem at similar rates in each image. This rate
        should be such that it proceeds far enough to avoid awkward angles between points, but also avoid travelling too far and missing key points
    2. get the topmost and bottommost points of the contour to set bounds
    3. find the first value of last_pt2 as step+1 from the bottom of the stem. This value can be decreased or increased by using the -i option to set
        the decimal fraction to multiply step by.
    4. find the first value of last_pt as 1 further from the bottom of the panicle than last_pt2
    5. set the initial value of bottom as last_pt2
    6. begin looping until a breakpoint has been found. This will follow the stem even if it is slightly angled or curved. Each loop will consist of:
      a. project next point as step pixels along the line through last_pt and last_pt2
      b. if the point projection errored or the point is within 3 steps of the top, the initial value of bottom will be returned and it will report
          no stem found
      c. find the slope between last_pt and point
      d. find the width at point
      e. if there is an error finding the width at this point, the initial value of bottom will be returned
      f. if there are less than 2 points found so far or the width falls within the mean_width, the width is included in the mean_width calculation
          and the loop continues (at least two points are needed here to calculate a proper average)
    7. if the bottom has been found, a check is run by continuing for 50 more steps along the line passing through the proposed bottom and the last 
        point before it. If the width at any of these points is found to be approximately stem width again, the bottom is moved.
  
  find_centerline() once the bottom point has been found:
    1. calculate the step as step_prep times the check_reach value. This should not be too small as it will cause the variability of the edges of the
        panicle contour to have too much influence over the lateral position of the next point. If the step value is too big, there will not be enough
        data
    2. a loop is begun to trace the path of the panicle:
      a. the center of mass point is found by using a section of the panicle between the last point and a point step pixels along the line passing through
          the last 2 points
      b. the distance between the last point and the center of mass point is calculated to ensure it is at least 1/4 of the step value. if the proposed
          point fails this test, the center of mass point is forced to 1/4 of the step value in the same direction from the last point
      c. if the proposed point is either 45 degrees to the left or right of the line passing through the last 2 points, or the proposed point falls
          outside of the contour, then a new projected point is placed at twice the check_reach value along the line passing through the last 2 points.
          The line passing between this point and the last point is checked for intersections with the contour. An intersection is used as the last
          point, a lack of intersection indicates an error.
      d. if the proposed point passes the 45 degree test (check_pt()) and the cv2.pointPolygonTest(), it is added to the centerline point list

# Calculation
  The calc_dimensions() method takes the data points found in the discovery elements of the program, and performs a few calculations on them to find
    data points of interest. The width is calculated as the average of the 3 widest points in the panicle. This is to avoid issues of extremes because 
    the sides of the panicle contour are uneven. The length is simply the sum of the distances between each of the points in the centerline point list.
    The area is the calculated by using a native opencv function that calculates the area of a contour. The average RGB values are calculated by masking
    off the image outside of the contour, then averaging the RGB values. Curvature is a little trickier. This value is calculated as the average change
    in angle from one point to the next for all points except the first and last points. These two points are excluded because it is easier for them to
    jump more quickly, increasing the possibility that they falsely influence the average.

  Data calculated:
    1. length: distance between successive points in the point list
    2. width: an average of the 3 widest widths along the panicle. Calculated by:
      a. calculate the width at each point in the point list
      b. sort the points by width
      c. average the 3 largest widths
    3. area: use the cv2.contourArea() function
    4. curvature: looping through all points in the centerline point list but the first and last:
      a. get the slope for the first point
      b. convert the slope to radians using np.arctan() (if the slope is None, meaning vertical, use np.inf as input to np.arctan()). It is important to
          convert any negative radian values to positive at this point by adding np.pi to the value. This will eliminate large averages for lines that 
          change slope from positive to negative, or vice versa, because this will create huge differences due to the nature of the arctan function
      c. repeat a + b for the second points slope
      d. add to angle_dif the absolute value of the difference between these angles divided by the distance between the two points (the division by 
          distance regulates by distance, and creates the proper units for curvature)
      e. once all the points have been processed, calculate the curvature as angle_dif divided by number of points divided by scale. This creates an
          average curvature that has correct units, mm^-1
    5. average RGB:
      a. mask the image outside of the contour
      b. use the opencv function mean() to find the average value of each color
      c. average each of the color averages

# Output
  write_calc() writes the calculated values to the end of the csv file that is opened at the beginning of the program and closed once each image has been
    processed.
  draw_elements() draws several of the measured points to the original image:
    1. the 3 widths used to calculate width
    2. panicle contour
    3. centerline points
    4. line and points indicating distance used for scale calculation
    5. once all the elements have been added, the image is shrunk by half then written to disk


