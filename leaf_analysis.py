#!/usr/bin/env python

import numpy as np
import cv2
import getopt
import os
import sys
import math
import zbar
from PIL import Image
import argparse

#### FUNCTION DEFINITIONS ####
def qr_read( f ):
    # create a reader
    scanner = zbar.ImageScanner()

    # configure the reader
    scanner.parse_config('enable')

    # obtain image data
    pil = Image.open(f).convert('L')
    width, height = pil.size
    raw = pil.tostring()
    
    # wrap image data
    image = zbar.Image(width, height, 'Y800', raw)

    # scan the image for barcodes
    scanner.scan(image)
    
    # extract results
    for symbol in image:
        return symbol.data

    # if no qrcode is found return ''
    return ''

# return mean bgr values
def mean_bgr( img, area ):
    # Split color channels
    b = cv2.split(img)[0]
    g = cv2.split(img)[1]
    r = cv2.split(img)[2]

    return (np.sum(b)/float(area), np.sum(g)/float(area), np.sum(r)/float(area)) 

# returns cosine of angle with a vertex at vert
def cosine( vert, pt2, pt3 ):
    # get lengths between points
    lv2 = math.hypot( vert[0] - pt2[0], vert[1] - pt2[1] )
    lv3 = math.hypot( vert[0] - pt3[0], vert[1] - pt3[1] )
    l23 = math.hypot( pt2[0] - pt3[0], pt2[1] - pt3[1] )

    return ((lv2**2 + lv3**2 - l23**2) / (2 * lv2 * lv3))

# returns scale value based on size of label
def calibrate_img( r,g):
    ##      Find the scale (calibration)
    # The label is used as the scale for calibration. Therefore, the following code isolates the label from the rest of
    # the image and finds the length of the label.
    calibration_img = cv2.subtract(r, g)   # This subtraction does a good job of isolating the manila folder + the label

    # Otsu's binarization, used here, is a dynamic threshold that does not used a fixed threshold value.
    ret, calibrationThresh = cv2.threshold(calibration_img, 0, 255, cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)

    # The image "calibrationThresh" is currently composed of a white label, inside a black manila folder, against a
    # white background. We want the label to be the only white thing in the image, so we can isolate it. To achieve
    # this, we floodfill the background black. The mask is unimportant, but the floodfill function needs it for some
    # reason.
    mask = cv2.copyMakeBorder(np.bitwise_not(calibrationThresh), 1, 1, 1, 1, cv2.BORDER_CONSTANT, (255, 255, 255))
    cv2.floodFill(calibrationThresh, mask, (2, 2), (0, 0, 0))

    # Ideally, the label is now the only white thing in the image. However, there are always random speckles here and
    # there. Therefore, we find all the contours and take the biggest rectangular-ish contour, which should end up being
    # the label, unless there happens to be a humongous rectangular speckle.
    calibrationContours, hierarchy = cv2.findContours(calibrationThresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    
    mar = ''
    mar_area = 0

    # find rectangles
    for contour in calibrationContours:
        # approximate polygonal curve 
        approx = cv2.approxPolyDP( contour, cv2.arcLength( contour, True ) * 0.02, True )

        temp_area = cv2.contourArea(approx)

        if len( approx ) == 4 and temp_area > 4000 and cv2.isContourConvex(approx) and mar_area < temp_area:
            maxCosine = 0.0
            mar_area = temp_area

            for i in xrange( 2,5 ):
                maxCosine = max( maxCosine, cosine( approx[i-1][0], approx[i%4][0], approx[i-2][0] ) )

            if maxCosine < 0.3:
                mar = cv2.minAreaRect(contour)
    
    return mar

# distance formula
def distance(p0, p1):
    return math.sqrt((p0[0] - p1[0])**2 + (p0[1] - p1[1])**2)

# use masked image input; find stem end of leaf; return 1 for stem down and 0 for stem up
def find_stem( mask ):
    mask1, mask2 = np.split( mask, [mask.shape[0]/2] )
    if np.sum(mask1) < np.sum(mask2):
        return False
    else:
        return True

###### START MAIN ######
def main(prog_name, argv):
    out_dir = os.getcwd() + os.sep + 'leaf_analysis_out'

    prog_name = os.path.basename( prog_name )

    # ARG PROCESSING
    parser = argparse.ArgumentParser( prog=prog_name, description='Process images to find the length, width, and area of the leaf in each image.',
            formatter_class=argparse.ArgumentDefaultsHelpFormatter )
    parser.add_argument('infile', metavar='INFILE', type=str, nargs='+', help='image files to be processed')
    parser.add_argument('-o','--out_dir', dest='_dir', metavar='DIR', type=str, default=out_dir, help='directory to save output to')
    parser.add_argument('-l','--lbl_length', dest='_len', metavar='LEN', type=int, default=4.445, help='length of the label in cm, used for scaling the image')
    parser.add_argument('-n','--normalize', dest='normalize', action='store_const', const=True, default=False, help='indicates whether to normalize color if images are formatted properly')
    parser.add_argument('-v','--verbose', dest='verbose', action='store_const', const=True, default=False, help='print additional information to screen while processing')
    parser.add_argument('-p','--perp_label', dest='perp_label', action='store_const', const=True, default=False, help='orient image based on perpendicular label')
    args = parser.parse_args(argv)

    # SET VARIBLES FROM COMMAND LINE ARGS
    lbl_len = args._len
    normalize = args.normalize
    out_dir = args._dir
    infile = args.infile
    verbose = args.verbose
    perp_label = args.perp_label

    # make sure out_dir path is absolute
    if not os.path.isabs(out_dir):
        out_dir = os.getcwd() + os.sep + out_dir

    # if out_dir doesn't exist, create it
    if not os.path.isdir(out_dir):
        if verbose:
            print( out_dir + ' created')
        os.makedirs(out_dir)
    
    # Open output csv file and initialize with header
    fh = open(out_dir + os.sep + 'leaf_analysis.csv', 'w')
    fh.write('#' + ', '.join(['Name', 'QR Code', 'Length', 'Width', 'Area', 'lbl_len', 'Scale', 'Label_Present', 'Label_File', 'Blue(L)', 'Green(L)', 'Red(L)']))
    if normalize:
        fh.write( ', ' + ', '.join(['Blue(L:E)', 'Green(L:E)', 'Red(L:E)', 'Blue(E)', 'Green(E)', 'Red(E)']) + '\n')  # for header 
    else:
        fh.write( '\n' )

    # set initial scale value
    scale = 66.0
    lbl_exists = True
    lbl_file = 'Initial_Value'
    # set crop distance
    crop_dist = 0

    #### PROCESS IMAGE FILES ####
    for f in infile:
        if verbose:
            print( 'Processing: ' + f)
        qr_code = qr_read( f )  # get qr_code from label

        if qr_code == '':
            print( f + ': Error: qr code not found')

        img = cv2.imread(f, 1)
        proc_img = img.copy()
        img_copy = img.copy()

        # Split color channels
        b = cv2.split(img)[0]
        g = cv2.split(img)[1]
        r = cv2.split(img)[2]

        # get minAreaRect() output for contour of the label
        lbl_mar = calibrate_img( r,g )
        
        lbl_mar_box = cv2.cv.BoxPoints(lbl_mar)
        lbl_mar_box = np.int0(lbl_mar_box)
        cv2.drawContours(img_copy, [lbl_mar_box], 0, 255, -1)
        cv2.imwrite( out_dir + os.sep + f.split(os.sep)[-1] + 'apple.jpg', img_copy )

        # break if data isn't returned from calibrate_img()
        if lbl_mar == '':
            print( 'Error finding label in image')
            lbl_exists = False
            w = None
        else:
            (_x,_y), (h,w), angle = lbl_mar
            scale = max(w,h)/lbl_len   # Scale is simply the number of pixels in one label-length / lbl_len which is the length of the label in cm
            lbl_exists = True
            lbl_file = f

        if normalize:
            #### MANILA ENVELOPE BGR FOR COLOR SCALE ####
            # get sample mean_bgr() from manila envelope
            lbl_mar_box = cv2.cv.BoxPoints(lbl_mar) 
            if w > h:
                # use y values
                lbl_idx = sorted( lbl_mar_box, key=lambda tup: tup[0] )
                mv_dist = max( math.fabs( lbl_idx[0][1] - _y ), math.fabs( lbl_idx[1][1] - _y) + 10)
                lbl_mar = ((lbl_mar[0][0],lbl_mar[0][1]-mv_dist), lbl_mar[1], lbl_mar[2])
            else:
                # use x values
                lbl_idx = sorted( lbl_mar_box, key=lambda tup: tup[1] )
                mv_dist = max( math.fabs( lbl_idx[0][0] - _x ), math.fabs( lbl_idx[1][0] - _x) + 10)
                lbl_mar = ((lbl_mar[0][0]-mv_dist,lbl_mar[0][1]), lbl_mar[1], lbl_mar[2])
            lbl_mar_box = cv2.cv.BoxPoints(lbl_mar) 
            lbl_mar_box = np.int0(lbl_mar_box)

            mask = np.zeros((img.shape[0],img.shape[1],1), np.uint8)
            cv2.drawContours(mask, [lbl_mar_box], 0, 255, -1)
            mask = cv2.bitwise_and( img, img, mask=mask )
            env_bgr = mean_bgr( mask, cv2.contourArea(lbl_mar_box) )
            
        ########################################
        ########### LEAF ANALYSIS ##############
        ########################################
        img = cv2.add(r, g)  # This does a good job of isolating the leaf
        img = cv2.blur(img, (13, 13))  # In some images, the clear glass on top of the leaf messes up the image such that
                                       # the leaf looks like two disconnected entities. The following code only works if
                                       # the leaf is one entity. Blurring helps "reconnect" the two pieces of the leaf.
                                       # The amount of blurring is controlled by the second parameter -- right now, it's
                                       # (13, 13), but that may need to be adjusted based on further testing.

        # Rotate image, if necessary, so that leaf is vertical
        # Need for rotation is determined by the alignment of the label, ie, if the label is horizontal, then the leaf
        # is also horizontal, and therefore rotation is necessary
        if w:
            if ((w > h or (w < h and angle > -45)) and not perp_label) or ((w < h or (w > h and angle > -45)) and perp_label):
                # Rotation procedure
                center = (img.shape[1]/2, img.shape[1]/2)
                M = cv2.getRotationMatrix2D(center, 90.0, 1.0)
                img = cv2.warpAffine(img, M, (img.shape[0], img.shape[1]))
                proc_img = cv2.warpAffine(proc_img, M, (img.shape[1], img.shape[0]))

                # Redefine the coordinates for the center of the label
                temp_x = _x
                _x = _y
                _y = img.shape[1] - temp_x  # _y is never used, but I include it for completeness of code
            
            crop_dist = min(w,h)/2 + _x
            img = img[:, crop_dist:]  # Crop out the part of the image to the left of the label, since the leaf is always
                                            # right of the label.

        ret, thresh = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
        
        # find all contours in the image after processing
        contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

        # Loop through all the contours and take the biggest one, as that is usually the leaf. This is where we may run into
        # trouble, since sometimes there are small oddities in the image that, if large enough, fool the program into
        # picking it as the leaf.
        if len(contours) == 0:
          print( 'Error: Could not find leaf')
          continue

        leafContour = contours[0]
        for contour in contours:
            if cv2.contourArea(contour) > cv2.contourArea(leafContour):
                leafContour = contour

        for p in leafContour:
            p[0][0] += crop_dist

        leaf_area = cv2.contourArea( leafContour )

        #### MASK LEAF ####
        mask = np.zeros((img_copy.shape[0],img_copy.shape[1],1), np.uint8)
        cv2.drawContours(mask, [leafContour], 0, 255, -1)
        mask = cv2.bitwise_and( img_copy, img_copy, mask=mask )
        leaf_bgr = mean_bgr( mask, leaf_area )

        # minAreaRect()
        mar = cv2.minAreaRect( leafContour )

        #### GET MIDPOINTS OF minAreaRect() WIDTH LINES ####
        mar_box = cv2.cv.BoxPoints(mar) 
        mar_mp = []  # midpoint list
        for i in xrange(4):
            mar_mp.append( (int((mar_box[i][0] + mar_box[(i+1)%4][0])/2), int((mar_box[i][1] + mar_box[(i+1)%4][1])/2)) )
        idx = sorted( mar_box, key=lambda tup: tup[1] )
        
        #### GET CENTER LINE ####
        if find_stem(mask):
            topmost = tuple(leafContour[leafContour[:, :, 1].argmin()][0])
            bottommost = (int((idx[2][0] + idx[3][0])/2), int((idx[2][1] + idx[3][1])/2))
        else:
            bottommost = tuple(leafContour[leafContour[:, :, 1].argmax()][0])
            topmost = (int((idx[0][0] + idx[1][0])/2), int((idx[0][1] + idx[1][1])/2))

        #### DRAW CONTOUR AND OTHER MEASURE LINES ####
        cv2.drawContours(proc_img, [np.int0(mar_box)], 0, (255, 0, 0), 12)  # draws box contour on proc_img
        cv2.line(proc_img, mar_mp[0], mar_mp[2], (255, 0, 0), 12)  # draws line on proc_img
        cv2.line(proc_img, mar_mp[1], mar_mp[3], (255, 0, 0), 12)  # draws line on proc_img
        cv2.line(proc_img, topmost, bottommost, (233, 100, 233), 8)  # draws line on proc_img
        cv2.circle(proc_img, (int(mar[0][0]), int(mar[0][1])), 1, (0,255,255), 15)
        cv2.drawContours(proc_img, [leafContour], 0, (255, 0, 255), 10)  # draws leaf on proc_img
        
        #### REDUCE IMAGE AND PRINT TO FILE ####
        proc_img = cv2.pyrDown( proc_img )
        proc_img = cv2.pyrDown( proc_img )
        filename = os.path.basename(f)
        filename = os.path.splitext(filename)[0]
        cv2.imwrite(out_dir + os.sep + filename + "_out.jpg", proc_img)

        #####################
        ## WRITE CSV OUTPUT ### 
        #########################
        length = distance(topmost, bottommost)

        fh.write(filename)

        ##  QR Code
        fh.write(', ' + qr_code)

        ##  Length
        fh.write(', ' + str(length/scale))

        ##  Width
        fh.write(', ' + str(mar[1][1]/scale))

        ##  Area
        fh.write(', ' + str((leaf_area)/(scale**2)))

        ##  lbl_length
        fh.write(', ' + str(lbl_len))

        ##  scale
        fh.write(', ' + str(scale))

        ##  Label Presence and File the scale is determined from
        fh.write(', ' + str(lbl_exists))
        fh.write(', ' + str(lbl_file))

        ##  Leaf BGR Values
        fh.write(', ' + str(leaf_bgr[0]) + ', ' + str(leaf_bgr[1]) + ', ' + str(leaf_bgr[2]))

        if normalize:
            ##  Leaf:Env BGR Values + Env BGR Values
            fh.write( ', ' + ', '.join([str(float(leaf_bgr[0])/env_bgr[0]),
                    str(float(leaf_bgr[0])/env_bgr[1]),
                    str(float(leaf_bgr[0])/env_bgr[2]),
                    str(env_bgr[0]),
                    str(env_bgr[1]),
                    str(env_bgr[2])]) + '\n')
        else:
            fh.write( '\n' )
            
    ## CLOSE CSV FILE ##
    fh.close()

if __name__ == '__main__':
    main(sys.argv[0], sys.argv[1:])
