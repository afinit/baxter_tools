#README#

# leaf_analysis #

## Installation of Dependencies for UNIX/Linux ##

### Python 2.7 ###

Pre-installed or available through package manager

### OpenCV ###

[Follow these instructions](http://docs.opencv.org/doc/tutorials/introduction/linux_install/linux_install.html)

### NumPy ###

`pip install numpy`

### Pillow ###

`pip install Pillow`

### Zbar ###

* `pip install zbar`

## Usage ##

    usage: leaf_analysis.py [-h] [-o,--out_dir DIR] [-l,--lbl_length LEN]
                            [-n,--normalize] [-v,--verbose]
                            INFILE [INFILE ...]

    Process images to find the length, width, and area of the leaf in each image.

    positional arguments:
      INFILE               image files to be processed

    optional arguments:
      -h, --help           show this help message and exit
      -o,--out_dir DIR     directory to save output to (default: ./leaf_analysis_out)
      -l,--lbl_length LEN  length of the label in cm, used for scaling the image (default: 4.445)
      -n,--normalize       indicates whether to normalize color if images are formatted properly (default: False)
      -v,--verbose         print additional information to screen while processing (default: False)

Examples (written to be run in a unix/linux environment):

* help: `leaf_analysis.py -h`
* wildcard selection of images: `leaf_analysis.py ./sla_images/*`
* specified output directory: `leaf_analysis.py -o ./sla_processed ./sla_images/*`
* single images with wildcard selection: `leaf_analysis.py --out_dir ./sla_processed ./sla_images/img1.jpg ./sla_images/img5*jpg ./sla_images/img2.jpg ./other_images/pic1.jpg`
* different label length: `leaf_analysis.py -l 2.01 -o ./sla_processed ./sla_images/*`


# panicle #

## Installation of Dependencies ##

### Python 2.7 ###

Pre-installed or available through package manager

### OpenCV ###

[Follow these instructions](http://docs.opencv.org/doc/tutorials/introduction/linux_install/linux_install.html)

### NumPy ###

`pip install numpy`

## Usage ##

    usage: panicle.py [-h] [-o,--out_dir DIR] [-s,--scale_key KEY]
                      [-p,--scale_pix PIXELS] [-l,--left_crop LCROP]
                      [-r,--right_crop RCROP] [-t,--top_crop TCROP]
                      [-b,--bottom_crop BCROP] [-m,--percent_mask PERC_MASK]
                      [-i,--init_step ISTEP] [-n,--no_stem] [-v,--verbose]
                      INFILE [INFILE ...]

    Process images to find the length, width, and area of the panicle in each
    image.

    positional arguments:
      INFILE                image files to be processed

    optional arguments:
      -h, --help            show this help message and exit
      -o,--out_dir DIR      directory to save output to (default: panicle_out)
      -s,--scale_key KEY    length of the scale in mm, used for scaling the image
                            (default: 20)
      -p,--scale_pix PIXELS
                            manual entry for scale length in pixels. this is
                            useful when the scale tape is measured wrong (default:
                            -1)
      -l,--left_crop LCROP  fraction of image to crop from the left side of the
                            image in decimal form (default: 0.0)
      -r,--right_crop RCROP
                            fraction of image to crop from the right side of the
                            image in decimal form (default: 0.0)
      -t,--top_crop TCROP   fraction of image to crop from the top side of the
                            image in decimal form (default: 0.0)
      -b,--bottom_crop BCROP
                            fraction of image to crop from the bottom side of the
                            image in decimal form (default: 0.0)
      -m,--percent_mask PERC_MASK
                            minimum percentage of the total image area to consider
                            a contour when attempting to mask out the sheet
                            present in most images (default: 0.15)

      -i,--init_step ISTEP  fraction of scaled step to take initially from the
                            bottom (default: 1.0)
      -n,--no_stem          skip process of finding stem and simply use the bottom
                            of the contour (default: False)
      -v,--verbose          print additional information to screen while
                            processing (default: False)

Examples (written to be run in a unix/linux environment):

* help:  panicle.py -h
* most basic input will work as leaf_analysis above
